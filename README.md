# README

A simple application that makes an API call and fetches data from the API and persists the data to a database

### Setup
* run 'docker-compose run web rails new . --force --no-deps --database=postgresql'
* run 'docker-compose build'
* Spin up the container with 'docker-compose up'
* Start a bash session in the container by running 'docker exec -ti containerName bash'
* Inside the container run the command 'rake db:create'
* followed by rake 'db:migrate'
***
You can now connect to the app at localhost:3000
