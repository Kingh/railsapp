FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /saicom
WORKDIR /saicom
COPY Gemfile /saicom/Gemfile
COPY Gemfile.lock /saicom/Gemfile.lock
RUN bundle install
COPY . /saicom

#Script to be executed every time the container starts
COPY scripts /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start main process
CMD ["rails", "server", "-b", "0.0.0.0"]
