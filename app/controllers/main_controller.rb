require 'rest-client'

class MainController < ApplicationController
  def index
    @users = User.all
  end

  def emma
    @user = User.where(first_name: 'Emma')
  end
end